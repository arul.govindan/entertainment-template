import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule} from '@ionic/angular';

import { CommoncontentsPage } from './commoncontents.page';

const routes: Routes = [
  {
    path: '',
    component: CommoncontentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // NavParams,
    RouterModule.forChild(routes)
  ],
  declarations: [CommoncontentsPage]
})
export class CommoncontentsPageModule {}
