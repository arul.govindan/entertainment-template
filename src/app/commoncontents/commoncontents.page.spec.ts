import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommoncontentsPage } from './commoncontents.page';

describe('CommoncontentsPage', () => {
  let component: CommoncontentsPage;
  let fixture: ComponentFixture<CommoncontentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommoncontentsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommoncontentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
