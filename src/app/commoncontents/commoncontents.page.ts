import { Component, OnInit } from '@angular/core';
// import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-commoncontents',
  templateUrl: './commoncontents.page.html',
  styleUrls: ['./commoncontents.page.scss'],
})
export class CommoncontentsPage implements OnInit {

  currFlag: any = '';
  contactDetail: any = new Array();
  constructor() {
    this.contactDetail = [
      {name: 'Phone', val: '+91-9486112513'},
      {name: 'Email', val: 'enquiry@skeintech.com'},
      {name: 'Website', val: 'www.skeintech.com'},
    ];
    this.currFlag = sessionStorage.getItem('fromPageFlag');
    console.log(this.currFlag);
  }

  ngOnInit() {
  }

  ionViewDidLeave() {
   sessionStorage.removeItem('fromPageFlag');
  }

}
