import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'categories', loadChildren: './categories/categories.module#CategoriesPageModule' },
  { path: 'trending', loadChildren: './trending/trending.module#TrendingPageModule' },
  { path: 'myplaylist', loadChildren: './myplaylist/myplaylist.module#MyplaylistPageModule' },
  { path: 'appsettings', loadChildren: './appsettings/appsettings.module#AppsettingsPageModule' },
  { path: 'musicplayer', loadChildren: './musicplayer/musicplayer.module#MusicplayerPageModule' },
  { path: 'commoncontents', loadChildren: './commoncontents/commoncontents.module#CommoncontentsPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
