import { Component, OnInit } from '@angular/core';
import { CommondatasourceService } from '../commondatasource.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  catgoriesArray: any;
  trendingArray: any;

  constructor(
    public datasource: CommondatasourceService,
    public navCntrl: NavController
  ) {
    this.catgoriesArray = this.datasource._getCategories();
    this.trendingArray = this.datasource._getTrendingSongs();
    console.log(this.catgoriesArray);
   }

  ngOnInit() {
  }

  /**
   * Method for seeing main pages from home pages
   * @param pageKey
   */
  seeAll(pageKey: any) {
    this.navCntrl.navigateRoot('tabs/' + pageKey);
  }

}
