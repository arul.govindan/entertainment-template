import { Component, OnInit } from '@angular/core';
import { CommonutilsService } from '../commonutils.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-myplaylist',
  templateUrl: './myplaylist.page.html',
  styleUrls: ['./myplaylist.page.scss'],
})
export class MyplaylistPage implements OnInit {

  constructor(
    private utils: CommonutilsService,
    private alertCntrl: AlertController
  ) { }

  ngOnInit() {
  }

  /**
   * Method for adding music to playlist
   */
  async addMusic() {
    // this.utils._showAlert('Upcoming Soon');
    const alert = await this.alertCntrl.create({
      header: 'Mastero Music',
      message: 'Upcoming Soon',
      buttons: ['OK']
    });
    await alert.present();
  }

}
