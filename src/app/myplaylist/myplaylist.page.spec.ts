import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyplaylistPage } from './myplaylist.page';

describe('MyplaylistPage', () => {
  let component: MyplaylistPage;
  let fixture: ComponentFixture<MyplaylistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyplaylistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyplaylistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
