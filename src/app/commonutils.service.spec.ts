import { TestBed } from '@angular/core/testing';

import { CommonutilsService } from './commonutils.service';

describe('CommonutilsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommonutilsService = TestBed.get(CommonutilsService);
    expect(service).toBeTruthy();
  });
});
