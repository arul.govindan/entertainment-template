import { Component, OnInit } from '@angular/core';
import { CommondatasourceService } from '../commondatasource.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {
  catgoriesArray: any;

  constructor(
    public datasource: CommondatasourceService,
    public navCntrl: NavController,
  ) {
    this._restoreCat();
    console.log(this.catgoriesArray);
   }

  ngOnInit() {
  }

  /**
   * Method for seeing main pages from home pages
   * @param pageKey
   */
  navigateTrending() {
    this.navCntrl.navigateRoot('tabs/trending');
  }

  _searchFromCat(event) {
    const searchString = event.target.value;
    this.catgoriesArray = this.catgoriesArray.filter((item) => {
      return (item.catName.toLowerCase().indexOf(searchString.toLowerCase()) > -1);
    });
  }

  _restoreCat() {
    this.catgoriesArray = this.datasource._getCategories();
  }


}
