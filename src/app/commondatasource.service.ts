import {  Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommondatasourceService {

  settingsOptionArray: any = [{
    listHead: String,
    options: [{
      optName: String,
      iconVal: String,
      pageName: String
    }]
  }];
  categoriesArray: any = [{
    catName: String,
    catTotCount: Number,
    catThumbnail: String
  }];
  TrendingArray: any = [{
    trendName: String,
    trendDesc: String,
    trendThumbnail: String,
    trendSource: String
  }];

  constructor() {}

  /**
   * Method for getting categories
   */
  _getCategories() {
    this.categoriesArray = [{
        catName: 'Comedy',
        catSubName: 'Hear comedy & have a smile',
        catTotCount: 60,
        catThumbnail: 'assets/imgs/theatre.svg',
        banThumbnail: 'assets/imgs/thumbnail/comedy.jpg'
      },
      {
        catName: 'Romance',
        catSubName: 'Be in love with awesome memories',
        catTotCount: 80,
        catThumbnail: 'assets/imgs/heart.svg',
        banThumbnail: 'assets/imgs/thumbnail/romance.jpg'
      },
      {
        catName: 'Action',
        catSubName: 'Think and act bolder enough',
        catTotCount: 100,
        catThumbnail: 'assets/imgs/action.svg',
        banThumbnail: 'assets/imgs/thumbnail/action.jpg'
      },
      {
        catName: 'Sports',
        catSubName: 'Energetic action make fit always',
        catTotCount: 50,
        catThumbnail: 'assets/imgs/trophy.svg',
        banThumbnail: 'assets/imgs/thumbnail/sports.jpg'
      },
      {
        catName: 'Technology',
        catSubName: 'Changes are simple and smarter',
        catTotCount: 110,
        catThumbnail: 'assets/imgs/light-bulb.svg',
        banThumbnail: 'assets/imgs/thumbnail/technology.jpg'
      },
      {
        catName: 'Science',
        catSubName: 'Learn in and around our atoms',
        catTotCount: 20,
        catThumbnail: 'assets/imgs/brain-and-head.svg',
        banThumbnail: 'assets/imgs/thumbnail/science.jpg'
      },
    ];
    return this.categoriesArray;
  }

  _getTrendingSongs() {
    this.TrendingArray = [{
        trendName: 'Noora Desik Kane',
        trendDesc: 'Hindi black buster',
        trendThumbnail: 'assets/imgs/thumbnail/song1.jpg',
        // tslint:disable-next-line:max-line-length
        trendSource: 'https://firebasestorage.googleapis.com/v0/b/audiostreamingapp-bc724.appspot.com/o/file_example_MP3_700KB.mp3?alt=media&token=d683c8ab-3671-4b0f-92fd-2169ad09a134'
      },
      {
        trendName: 'Unavida Intha ula',
        trendDesc: 'Romantic melody tracker',
        trendThumbnail: 'assets/imgs/thumbnail/song2.jpg',
        // tslint:disable-next-line:max-line-length
        trendSource: 'https://firebasestorage.googleapis.com/v0/b/audiostreamingapp-bc724.appspot.com/o/file_example_MP3_700KB.mp3?alt=media&token=d683c8ab-3671-4b0f-92fd-2169ad09a134'
      },
      {
        trendName: 'Mere there Beach me',
        trendDesc: 'SPB Evergereen Hindi Hits',
        trendThumbnail: 'assets/imgs/thumbnail/song3.jpg',
        // tslint:disable-next-line:max-line-length
        trendSource: 'https://firebasestorage.googleapis.com/v0/b/audiostreamingapp-bc724.appspot.com/o/file_example_MP3_700KB.mp3?alt=media&token=d683c8ab-3671-4b0f-92fd-2169ad09a134'
      },
      {
        trendName: 'Koodi usuru kottuthe',
        trendDesc: 'Superb melody from Mehandi Circus',
        trendThumbnail: 'assets/imgs/thumbnail/song4.jpg',
        // tslint:disable-next-line:max-line-length
        trendSource: 'https://firebasestorage.googleapis.com/v0/b/audiostreamingapp-bc724.appspot.com/o/file_example_MP3_700KB.mp3?alt=media&token=d683c8ab-3671-4b0f-92fd-2169ad09a134'
      },
      {
        trendName: 'kannamochi rere',
        trendDesc: 'Jayam Ravi Evergreen song',
        trendThumbnail: 'assets/imgs/thumbnail/song5.jpg',
        // tslint:disable-next-line:max-line-length
        trendSource: 'https://firebasestorage.googleapis.com/v0/b/audiostreamingapp-bc724.appspot.com/o/file_example_MP3_700KB.mp3?alt=media&token=d683c8ab-3671-4b0f-92fd-2169ad09a134'
      }
    ];
    return this.TrendingArray;

  }

  /**
   * Method for getting settings option
   */
  _getSettings() {
    this.settingsOptionArray = [{
      listHead: 'Customizations',
      options: [{
          optName: 'Profile',
          iconVal: 'contact',
          pageName: 'profile'
        },
        {
          optName: 'Change Password',
          iconVal: 'key',
          pageName: 'changepassword'
        },
        {
          optName: 'Dark Mode',
          iconVal: 'moon',
          pageName: 'darktheme'
        },
      ]
    }, {
      listHead: 'Informations',
      options: [{
          optName: 'About',
          iconVal: 'information-circle',
          pageName: 'about'
        },
        {
          optName: 'Contact',
          iconVal: 'call',
          pageName: 'contact'
        },
        // {
        //   optName: 'Feedback',
        //   iconVal: 'chatbubbles',
        //   pageName: 'about'
        // },
        {
          optName: 'Privacy Policy',
          iconVal: 'paper',
          pageName: 'pripolicy'
        },
        {
          optName: 'Terms & Condition',
          iconVal: 'medal',
          pageName: 'terconditions'
        }
      ]
    }, {
      listHead: 'App',
      options: [{
          optName: 'Version - 1.0.0',
          iconVal: 'list',
          pageName: 'home'
        },
        {
          optName: 'Logout',
          iconVal: 'log-out',
          pageName: 'home'
        }
      ]
    }];
    return this.settingsOptionArray;
  }
}