import { Component, OnInit } from '@angular/core';
import { CommondatasourceService } from '../commondatasource.service';
import { NavController, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-appsettings',
  templateUrl: './appsettings.page.html',
  styleUrls: ['./appsettings.page.scss'],
})
export class AppsettingsPage implements OnInit {

  settingOptions: any;
  constructor(
    public datasource: CommondatasourceService,
    public alertCntrl: AlertController,
    public navCntrl: NavController
    ) {
    this.settingOptions = this.datasource._getSettings();
    console.log(this.settingOptions);
  }

  ngOnInit() {
  }

  /**
   * Method for navigating to pages
   * @param pagename
   */
  navigatePages(pagename: any) {
    const comContents = ['about', 'contact', 'pripolicy', 'terconditions'];
    const pageName: any = pagename;

    if (comContents.indexOf(pageName) !== -1) {
      this.navCntrl.navigateForward('/commoncontents');
      sessionStorage.setItem('fromPageFlag', pageName);
    } else {
      this.showAlert();
    }
  }

  /**
   * Method for adding music to playlist
   */
  async showAlert() {
    // this.utils._showAlert('Upcoming Soon');
    const alert = await this.alertCntrl.create({
      header: 'Mastero Music',
      message: 'Upcoming Soon',
      buttons: ['OK']
    });
    await alert.present();
  }

}
