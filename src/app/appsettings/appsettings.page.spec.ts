import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsettingsPage } from './appsettings.page';

describe('AppsettingsPage', () => {
  let component: AppsettingsPage;
  let fixture: ComponentFixture<AppsettingsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppsettingsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsettingsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
