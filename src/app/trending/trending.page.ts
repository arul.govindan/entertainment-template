import {  Component,  OnInit} from '@angular/core';
import {  CommondatasourceService
} from '../commondatasource.service';
import {  NavController} from '@ionic/angular';
import {  StreamingMedia,  StreamingAudioOptions} from '@ionic-native/streaming-media/ngx';

import * as firebase from 'firebase';
import {  environment} from '../../environments/environment';

@Component({
  selector: 'app-trending',
  templateUrl: './trending.page.html',
  styleUrls: ['./trending.page.scss'],
})
export class TrendingPage implements OnInit {

  trendingArray: any = new Array();
  tempFirebaseArray: any = new Array();

  constructor(
    public datasource: CommondatasourceService,
    public navCntrl: NavController) {
    firebase.initializeApp(environment.firebase);
    // this.trendingArray = this.datasource._getTrendingSongs();
  }

  ionViewDidEnter() {
    this.trendingArray = [];
    this.tempFirebaseArray = [];
    this.getStorageFiles();
  }

  ionViewDidLeave() {
    this.trendingArray = [];
    this.tempFirebaseArray = [];
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.getStorageFiles();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }

  ngOnInit() {}

  getStorageFiles() {
    this.trendingArray = [];
    this.tempFirebaseArray = [];
    const storageRef = firebase.storage().ref('audioFiles');
    storageRef.listAll().then((result: any) => {
      console.log(result.items);
      this.pushData(result.items).then(() => {
        this.trendingArray = this.tempFirebaseArray;
        console.log(this.trendingArray);
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  pushData(resultVal) {
    const albumNames = ['Noora Desik Kane', 'Unavida Intha ula', 'Mere there Beach me', 'Koodi usuru kottuthe', 'kannamochi rere'];
    // tslint:disable-next-line:max-line-length
    const albumDesc = ['Jayam Ravi Evergreen song', 'Superb melody from Mehandi Circus', 'SPB Evergereen Hindi Hits', 'Romantic melody tracker', 'Hindi black buster'];
    // tslint:disable-next-line:max-line-length
    const albumImages = ['assets/imgs/thumbnail/song1.jpg', 'assets/imgs/thumbnail/song2.jpg', 'assets/imgs/thumbnail/song3.jpg', 'assets/imgs/thumbnail/song4.jpg', 'assets/imgs/thumbnail/song5.jpg'];
    return new Promise((resolve) => {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < resultVal.length; i++) {
        // tslint:disable-next-line:max-line-length
        const urlString: any = 'https://firebasestorage.googleapis.com/v0/b/' + resultVal[i].location.bucket + '/o/audioFiles%2F' + resultVal[i].location.path.split('/')[1] + '?alt=media';
        this.tempFirebaseArray.push({
          trendSource: urlString,
          trendName: albumNames[Math.floor(Math.random() * albumNames.length)],
          trendDesc: albumDesc[Math.floor(Math.random() * albumDesc.length)],
          trendThumbnail: albumImages[Math.floor(Math.random() * albumImages.length)],
        });
      }
      resolve(true);
    });
  }

  /**
   * Method for playing music from the selected list
   * @param item
   */
  playItems(item: any) {
    sessionStorage.setItem('currentMusicTrack', JSON.stringify(item));
    this.navCntrl.navigateForward('musicplayer');
  }

  _searchFromTrend(event) {
    const searchString = event.target.value;
    this.trendingArray = this.trendingArray.filter((item) => {
      return (item.trendName.toLowerCase().indexOf(searchString.toLowerCase()) > -1);
    });
  }

  _restoreTrend() {
    this.getStorageFiles();
  }
}
