import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TrendingPage } from './trending.page';
import { StreamingMedia} from '@ionic-native/streaming-media/ngx';

const routes: Routes = [
  {
    path: '',
    component: TrendingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TrendingPage],
  providers: [StreamingMedia]
})
export class TrendingPageModule {}
