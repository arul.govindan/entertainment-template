import {  Component,  OnInit} from '@angular/core';
import {  StreamingMedia,  StreamingAudioOptions} from '@ionic-native/streaming-media/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { Platform, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-musicplayer',
  templateUrl: './musicplayer.page.html',
  styleUrls: ['./musicplayer.page.scss'],
})
export class MusicplayerPage implements OnInit {

  currentMusicTrack: any;
  title: any;
  artist: any;
  image = '';
  filename: any = '';
  duration: any = -1;
  currPlayingFile: MediaObject;
  storageDirectory: any;
  playThetrack = '';
  position: any = 0;
  getPositionInterval: any;
  isPlaying = false;
  isInPlay = false;
  isReady = false;

  getDurationInterval: any;
  displayPosition: any = '00:00';
  displayDuration: any = '00:00';

 constructor(
    private platform: Platform,
    public alertCntrl: AlertController,
    private media: Media) {
    const currTrack = sessionStorage.getItem('currentMusicTrack');
    this.currentMusicTrack = JSON.parse(currTrack);
    this.image = this.currentMusicTrack.trendThumbnail;
    this.playThetrack = this.currentMusicTrack.trendSource;
    this.filename = this.currentMusicTrack.trendName;
    console.log(this.currentMusicTrack);
  }

  ngOnInit() {
  }

  ionViewDidLeave() {
    sessionStorage.removeItem('currentMusicTrack');
  }

  ionViewDidEnter() {
    // comment out the following line when adjusting UI in browsers
    this.prepareAudioFile();
  }

  prepareAudioFile() {
    this.platform.ready().then((res) => {
      console.log('res', res);
      this.getDuration();
    });
  }

  getDuration() {
    console.log('get duration');
    this.currPlayingFile = this.media.create(this.playThetrack);
    console.log(this.currPlayingFile);
    this.currPlayingFile.play();
    this.currPlayingFile.setVolume(0.0);
    const self = this;

    let tempDuration = self.duration;
    this.getDurationInterval = setInterval(() => {
      if (self.duration === -1 || !self.duration) {
        // tslint:disable-next-line:no-bitwise
        self.duration = ~~(self.currPlayingFile.getDuration());
      } else {
        if (self.duration !== tempDuration) {
          tempDuration = self.duration;
        } else {
          self.currPlayingFile.stop();
          self.currPlayingFile.release();

          clearInterval(self.getDurationInterval);
          this.displayDuration = this.toHHMMSS(self.duration);
          self.setToPlayback();
        }
      }
    }, 100);
  }

  setToPlayback() {
    this.currPlayingFile = this.media.create(this.playThetrack);
    this.currPlayingFile.onStatusUpdate.subscribe(status => {
      switch (status) {
        case 1:
          break;
        case 2:
          this.isPlaying = true;
          break;
        case 3:
          this.isPlaying = false;
          break;
        case 4:
        default:
          this.isPlaying = false;
          break;
      }
    });
    this.isReady = true;
    this.getAndSetCurrentAudioPosition();
  }

  getAndSetCurrentAudioPosition() {
    const diff = 1;
    const self = this;
    this.getPositionInterval = setInterval(() => {
      const lastPosition = self.position;
      self.currPlayingFile.getCurrentPosition().then((position) => {
        if (position >= 0 && position < self.duration) {
          if (Math.abs(lastPosition - position) >= diff) {
            self.currPlayingFile.seekTo(lastPosition * 1000);
          } else {
            self.position = position;
            this.displayPosition = this.toHHMMSS(self.position);
          }
        } else if (position >= self.duration) {
          self.stop();
          self.setToPlayback();
        }
      });
    }, 100);
  }

  play() {
    this.currPlayingFile.play();
  }

  pause() {
    this.currPlayingFile.pause();
  }

  stop() {
    this.currPlayingFile.stop();
    this.currPlayingFile.release();
    clearInterval(this.getPositionInterval);
    this.position = 0;
  }

  controlSeconds(action) {
    const step = 5;
    const numberRange = this.position;
    switch (action) {
      case 'back':
        this.position = numberRange < step ? 0.001 : numberRange - step;
        break;
      case 'forward':
        this.position = numberRange + step < this.duration ? numberRange + step : this.duration;
        break;
      default:
        break;
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.stop();
  }

  toHHMMSS(secs) {
    const secNum = parseInt(secs, 10);
    const minutes = Math.floor(secNum / 60) % 60;
    const seconds = secNum % 60;
    return [minutes, seconds]
      .map(v => v < 10 ? '0' + v : v)
      .filter((v, i) => v !== '00' || i >= 0)
      .join(':');
  }

  /**
   * Method for adding music to playlist
   */
  async showAlert() {
    // this.utils._showAlert('Upcoming Soon');
    const alert = await this.alertCntrl.create({
      header: 'Mastero Music',
      message: 'Upcoming Soon',
      buttons: ['OK']
    });
    await alert.present();
  }

}
