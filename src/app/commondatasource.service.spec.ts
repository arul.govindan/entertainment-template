import { TestBed } from '@angular/core/testing';

import { CommondatasourceService } from './commondatasource.service';

describe('CommondatasourceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommondatasourceService = TestBed.get(CommondatasourceService);
    expect(service).toBeTruthy();
  });
});
