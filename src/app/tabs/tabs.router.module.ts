import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'categories',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../categories/categories.module').then(m => m.CategoriesPageModule)
          }
        ]
      },
      {
        path: 'trending',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../trending/trending.module').then(m => m.TrendingPageModule)
          }
        ]
      },
      {
        path: 'myplaylist',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../myplaylist/myplaylist.module').then(m => m.MyplaylistPageModule)
          }
        ]
      },
      {
        path: 'appsettings',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../appsettings/appsettings.module').then(m => m.AppsettingsPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
